import React, { Component } from 'react';
import logo1 from './logo1.png';
import firebase, { auth } from './firebase';
import Navbar from './Navbar';
import { Modal } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

export default class Login extends Component {
    constructor() {
        super();
        this.state = {
            resname: [],
            filter: [],
            items: [],
            User: [],
            ResOwner: [],
            username: [],
            status: [],
            item_id: '',
            title: '',
            name: [],
            description: '',
            address: '',
            email: [],//add email state
            password: [],//add password state
            isLogin: true, //add isLogin state
            showView: false
        }

        this.login = this.login.bind(this)
    }

    componentDidMount() {

        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ user })
            }
        })

        const UserRef = firebase.database().ref('User');
        UserRef.on('value', (snapshot) => {
            let User = snapshot.val();
            let newState = [];
            for (let item in User) {
                newState.push({
                    item_id: item,
                    name: User[item].name,
                    email: User[item].email,
                    username: User[item].username,
                    address: User[item].address,
                    status: User[item].status

                })
            }
            this.setState({
                User: newState
            })
        })
    }
    login = () => {

        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {

            this.setState({ isLogin: true });

        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode === 'auth/wrong-password') {
                alert('Wrong password.');
            } else {
                alert(errorMessage);
            }
            console.log(error);
        });
    }


    render() {
        return (
            <div>
                <main role="main" className="container " style={{ marginTop: 80 }}>
                    <div className="container img">
                        <img src={logo1} width="500px" height="325px" alt="A logo1" alignitem />
                    </div>
                    <div className="row">
                        <div className="col-4"></div>
                        <div className="col-4">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" name="email" onChange={this.handleChange} value={this.state.email} placeholder="Enter email" />
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" name="password" onChange={this.handleChange} value={this.state.password} placeholder="Password" />
                                </div>
                                <Link to='/Home'>
                                    <button class="btn btn-primary">Log In</button>
                                    {/* <button class="btn btn-primary" onClick={() => this.login()}>Log In</button> */}
                                </Link>
                            </form>
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}
