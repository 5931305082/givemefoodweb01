import React, { Component } from 'react';
import logo1 from './logo.jpg';
import firebase, { auth } from './firebase';




class App extends Component {

  constructor() {
    super();
    this.state = {
      resname: [],
      filter: [],
      items: [],
      item_id: '',
      title: '',
      name: '',
      description: '',
      email: '',//add email state
      password: '',//add password state
      isLogin: true //add isLogin state
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)

  }

  componentDidMount() {

    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user })
      }
    })

    const itemsRef = firebase.database().ref('items');
    itemsRef.on('value', (snapshot) => {
      let items = snapshot.val();
      let newState = [];
      for (let item in items) {
        newState.push({
          item_id: item,
          title: items[item].title,
          description: items[item].description
        })
      }
      this.setState({
        items: newState
      })
    })
    const resnameRef = firebase.database().ref('resname');
    resnameRef.on('value', (snapshot) => {
      let resname = snapshot.val();
      let newState = [];
      for (let item in resname) {
        newState.push({
          item_id: item,
          title: resname[item].title,
          description: resname[item].description
        })
      }
      this.setState({
        resname: newState
      })
    })
    const filterRef = firebase.database().ref('Filter');
    filterRef.on('value', (snapshot) => {
      let filter = snapshot.val();
      let newState = [];
      for (let item in filter) {
        newState.push({
          item_id: item,
          title: filter[item].title,
          description: filter[item].description
        })
      }
      this.setState({
        filter: newState
      })
    })

  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }


  handleSubmit(e) {
    e.preventDefault();

    if (this.state.item_id !== '') {
      return this.updateItem();
    }

    const itemsRef = firebase.database().ref('resname')
    const item = {
      title: this.state.title,
      description: this.state.description
    }
    itemsRef.push(item)
    this.setState({
      item_id: '',
      title: '',
      description: ''
    })
  }

  handleUpdate = (item_id = null, title = null, description = null) => {
    this.setState({ item_id, title, description })
  }

  updateItem() {

    var obj = { title: this.state.title, description: this.state.description }

    const itemsRef = firebase.database().ref('/resname')

    itemsRef.child(this.state.item_id).update(obj);

    this.setState({
      item_id: '',
      title: '',
      description: ''
    })

  }

  removeItem(itemId) {
    const itemsRef = firebase.database().ref('/items');
    itemsRef.child(itemId).remove();
  }
  removeItem1(itemId) {
    const itemsRef = firebase.database().ref('/resname');
    itemsRef.child(itemId).remove();
  }

  login = () => {

    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {

      this.setState({ isLogin: true });

    }).catch(function (error) {
      var errorCode = error.code;
      var errorMessage = error.message;
      if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
    });
  }

  logout() {
    auth.signOut().then(() => {
      this.setState({ isLogin: false })
    })
  }


  loginForm() {
    return (
      <div>
        <main role="main" className="container" style={{ marginTop: 80 }}>
          <div className="row">
            <div className="col-4"></div>
            <div className="col-4">
              <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" name="email" onChange={this.handleChange} value={this.state.email} placeholder="Enter email" />
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="password" onChange={this.handleChange} value={this.state.password} placeholder="Password" />
                </div>
                <button class="btn btn-primary" onClick={() => this.login()}>Log In</button>
              </form>
            </div>
          </div>
        </main>
      </div>
    )
  }

  render() {


    if (!this.state.isLogin) {
      return this.loginForm()
    }


    return (
      <div>
        <nav class="navbar navbar-light bg-warning">
      <img src={logo1} width="80px" height="55px" alt="My logo" />
        <span class="navbar-brand mb-0 h1">Givemefood</span>
        {
          this.state.isLogin ? <button className="btn btn-danger" onClick={() => this.logout()} >Logout</button> : null

        }
        <button>Manage</button>
          </nav>

     
      <div className="content col-md-9">

        <div className="container" style={{ marginTop: 70 }}>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-8">
                <div className="form-row">

                </div>
              </div>
            </div>
          </form>
          <hr />

          <table className="table table-sm table-bordered">

            <tr className="thead-dark">

              <th width="70%">Description</th>
              <th width="20%">Delete</th>
              <th width="20">ADD</th>
            </tr>
            {
              this.state.filter.map((item) => {
                return (
                  <tr>
                    <td>{item.title}</td>
                    <td>{item.description}</td>

                    <td><button className="btn btn-warning btn-sm" onClick={() => this.handleUpdate(item.item_id, item.title, item.description)}>ADD</button></td><td><button className="btn btn-danger btn-sm" onClick={() => this.removeItem(item.item_id)}>ADD</button></td>
                    <td><button className="btn btn-danger btn-sm" onClick={() => this.removeItem(item.item_id)}>Delete</button></td>
                  </tr>
                )
              })
            }
          </table>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-8">
                <div className="form-row">
                  <div className="col-4">
                    <input type="text" name="title" className="form-control" placeholder="Title" onChange={this.handleChange} value={this.state.title} />
                  </div>
                  <div className="col-6">
                    <input type="text" name="description" className="form-control" placeholder="Description" onChange={this.handleChange} value={this.state.description} />
                  </div>
                  <div className="col">
                    <button class="btn btn-primary" > Confirm</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      </div >
    );
  }
}

export default App;