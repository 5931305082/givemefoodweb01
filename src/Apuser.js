import React, { Component } from 'react';
import Toggle from 'react-bootstrap-toggle';
import logo1 from './logo1.png';
import firebase, { auth } from './firebase';
import './App.css';
import Tableuser from "./Tableuser.1";
import isHidden from './Hideable'


import { Modal } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

class Apuser extends Component {

  constructor() {
    super();
    this.state = {
      resname: [],
      filter: [],
      items: [],
      User: [],
      ResOwner: [],
      phone:[],
      promotion:[],
      userName: '',
      ownerId:[],
      imageUrl:[],
      deliverycost:[],
      time:[],
      status: '',
      item_id: '',
      title: [],
      name: '',
      description: '',
      address: '',
      email: '',//add email state
      password: '',//add password state
      isLogin: true, //add isLogin state
      showView: false
    }

    this.replaceModalItem = this.replaceModalItem.bind(this);
    this.handleChange = this.handleChange.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    

    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)

  }

  componentDidMount() {

    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user })
      }
    })

    const UserRef = firebase.database().ref('User');
    UserRef.on('value', (snapshot) => {
      let User = snapshot.val();
      let newState = [];
      for (let item in User) {
        newState.push({
          item_id: item,
          username: User[item].username,
          name: User[item].name,
          email: User[item].email,
          address: User[item].address,
          status: User[item].state,
          deliverycost: User[item].deliverycost
        })
      }
      this.setState({
        User: newState
      })
    })
    const restaurant = firebase.database().ref('restaurant');
    restaurant.on('value', (snapshot) => {
      let restaurant = snapshot.val();
      let newState = [];
      for (let item in restaurant) {
        newState.push({
          item_id: item,
          title: restaurant[item].title,
          ownerId: restaurant[item].ownerId,
          phone: restaurant[item].phone,
          promotion: restaurant[item].promotion,
          deliverycost: restaurant[item].deliverycost,
          time: restaurant[item].time,
          imageUrl: restaurant[item].imageUrl
        })
      }
      this.setState({
        ResOwner: newState
      })
    })
    // firebaesของ RestaurantOwner
    // const ResOwnerRef = firebase.database().ref('RestaurantOwner');
    // ResOwnerRef.on('value', (snapshot) => {
    //   let ResOwner = snapshot.val();
    //   let newState = [];
    //   for (let item in ResOwner) {
    //     newState.push({
    //       item_id: item,
    //       username: ResOwner[item].username,
    //       name: ResOwner[item].name,
    //       email: ResOwner[item].email,
    //       address: ResOwner[item].address,
    //       status: ResOwner[item].status
    //     })
    //   }
    //   this.setState({
    //     ResOwner: newState
    //   })
    // })


    const filterRef = firebase.database().ref('Filter');
    filterRef.on('value', (snapshot) => {
      let filter = snapshot.val();
      let newState = [];
      for (let item in filter) {
        newState.push({
          item_id: item,
          title: filter[item].title,
          description: filter[item].description
        })
      }
      this.setState({
        filter: newState
      })
    })

  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }


  handleSubmit(e) {
    e.preventDefault();

    if (this.state.item_id !== '') {
      return this.updateItem();
    }

    const itemsRef = firebase.database().ref('resname')
    const item = {
      title: this.state.title,
      description: this.state.description
    }
    itemsRef.push(item)
    this.setState({
      item_id: '',
      title: '',
      description: ''
    })
  }

  handleUpdate = (item_id = null, title = null, description = null) => {
    this.setState({ item_id, title, description })
  }

  updateItem() {

    var obj = { title: this.state.title, description: this.state.description }

    const itemsRef = firebase.database().ref('/resname')

    itemsRef.child(this.state.item_id).update(obj);

    this.setState({
      item_id: '',
      title: '',
      description: ''
    })

  }


  replaceModalItem(index) {
    this.setState({
      requiredItem: index
    });
  }

  saveModalDetails(item) {
    const requiredItem = this.state.requiredItem;
    let tempbrochure = this.state.brochure;
    tempbrochure[requiredItem] = item;
    this.setState({ brochure: tempbrochure });
  }

  deleteItem(index) {
    let tempBrochure = this.state.brochure;
    tempBrochure.splice(index, 1);
    this.setState({ brochure: tempBrochure });
  }

  removeItem(itemId) {
    alert("Remove " + itemId)
    const itemsRef = firebase.database().ref('User');
    itemsRef.child(itemId).remove();
  }
  // viewItem(itemId) {
  //   const itemsRef = firebase.database().ref('/resname');
  //   itemsRef.child(itemId).vi();
  // }



  login = () => {

    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {

      this.setState({ isLogin: true });

    }).catch(function (error) {
      var errorCode = error.code;
      var errorMessage = error.message;
      if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
    });
  }

  logout() {
    auth.signOut().then(() => {
      this.setState({ isLogin: false })
    })
  }


  loginForm() {
    return (
      <div>
      <main role="main" className="container " style={{ marginTop: 80 }}>
        <div className="container img">
          <img src={logo1} width="500px" height="325px" alt="A logo1" alignitem />
        </div>
        <div className="row">
          <div className="col-4"></div>
          <div className="col-4">
            <form>
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" name="email" onChange={this.handleChange} value={this.state.email} placeholder="Enter email" />
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="password" onChange={this.handleChange} value={this.state.password} placeholder="Password" />
              </div>
              <button class="btn btn-primary" onClick={() => this.login()}>Log In</button>
            </form>
          </div>
        </div>
      </main>
    </div>
    )
  }




  toggleIsHidden() {
    this.setSate((currenState) => ({
      isHidden: !currenState.isHidden,
    }));
  }

  showViewModal(item) {
    this.setState({
      showView: true,
      title: item.title,
      phone: item.phone,
      deliverycost: item.deliverycost,
      promotion: item.promotion,
      userName: item.user,
      ownerId: item.ownerId,
      imageUrl: item.imageUrl,
      time: item.time,

    })
  }

  closeViewModal() {
    this.setState({
      showView: false
    })
  }



  render() {
    if (!this.state.isLogin) {
      return this.loginForm()
    }


    return (


      <div>

        <nav class="navbar navbar-light bg-warning fixed-top second-navbar">
          <ul className="navbar-nav mr-auto add-rmargin"></ul>
          {/* <img src={logo1} width="250px" height="150px" alt="A logo1" /> */}
          {/* <div className="add-rmargin"> */}
          <div  className="nav-link" style={{ marginRight: 1150 }}>Customer Manage</div>

          {/* <Link to={'./Home'} className="nav-link">User Manage</Link>
          <Link to={'./Apuser'} className="nav-link">Restaurant Manage</Link>
          <Link to={'./Tableuser'} className="nav-link">Add Admin </Link> */}
          {/* </div> */}

        </nav>



         <nav class="navbar  fixed-top  " >
          <img src={logo1} width="150px" height="70px" alt="A logo1" />

          {/* <span class="navbar-brand mr-0 h1" className="App-title" style={{ fontFamily: 'Lobster' }}>Givemefood</span> */}
          <div >
          <Link to={'./Home'} className="navbar-brand m-0 h1"  >Customer Manage</Link>
          <Link to={'./Apuser'} className="navbar-brand  m-0 h1" >Restaurant Manage</Link>
         </div>
         <button> 
            <Link onClick={() => this.Tableuser} className="btn btn-danger margin-c"  >Create Admin</Link>
            </button>
         
          {
            this.state.isLogin ? <button className="btn btn-danger" onClick={() => this.logout()} >Logout</button> : null
          }
          

        </nav>


        <div className="content col-md-9">

          <div className="container" style={{ marginTop: 150 }}>
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-8">
                  <div className="form-row">

                  </div>
                </div>
              </div>
            </form>
            <hr />

            <h1></h1>





            <Modal class="Modal" show={this.state.showView} size="lg"  onHide={() => this.closeViewModal()}>


              <Modal.Body style={{ marginTop: 10, width: 800 }}>
              
                <div style={{ width: 800 }}>
                  <h3>{this.state.title}</h3>
                  <div className="content col-md-9"></div>

              
                 
                  <div className="content col-md-9">

                    <div className="modal-body" value={this.state.item}>
                      <p><span className="modal-lable">Title:</span><input value={this.state.title} onChange={(e) => this.titleHandler(e)} /></p>
                      <p><span className="modal-lable">Owner ID:</span><input value={this.state.ownerId} onChange={(e) => this.titleHandler(e)} /></p>
                      <p><span className="modal-lable">Phone Number:</span><input value={this.state.phone} onChange={(e) => this.titleHandler(e)} /></p>
                      <p><span className="modal-lable">Image URL:</span><input value={this.state.imageUrl} onChange={(e) => this.titleHandler(e)} /></p>
                      <p><span className="modal-lable">Delivery Cost:</span><input value={this.state.deliverycost} onChange={(e) => this.titleHandler(e)} /></p>
                      <p><span className="modal-lable">Promotion:</span><input value={this.state.promotion} onChange={(e) => this.titleHandler(e)} /></p>
                      <p><span className="modal-lable">Time:</span><input value={this.state.time} onChange={(e) => this.titleHandler(e)} /></p>
                    </div>
                    {/* </div> */}
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer>
                
                <button className="btn btn-warning btn-sm" onClick={() => this.handleUpdate()}>Save</button>
                <button className="btn btn-danger btn-sm" onClick={() => this.closeViewModal()}>Close</button>
              </Modal.Footer>
            </Modal>




            <table className="table table-sm table-bordered">
              <tr className="thead-dark">
                <th width="5%">No.</th>
                <th width="20%">Title</th>
                <th width="15%">Phone</th>
                <th width="15%">Promotion</th>
                {/* <th width="5%">Edit</th> */}
                <th width="5%">Edit</th>

              </tr>
              {
                this.state.ResOwner.map((item, i) => {
                  return (
                    <tr key={i}>
                      <td>
                        {i + 1}
                      </td>
                      <td>{item.title}</td>
                      <td>{item.phone}</td>
                      <td>{item.promotion}</td>
                      {/* <td><button className="btn btn-warning btn-sm" onClick={() => this.showViewModal(item)}>edit</button></td> */}
                      <td><button className="btn btn-danger btn-sm" onClick={() => this.showViewModal(item)}>Edit</button></td>
                    </tr>
                  )
                })
              }
            </table>



          </div>

        </div>
      </div >
    );
  }
}

export default Apuser;


















