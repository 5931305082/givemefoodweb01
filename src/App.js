import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';

import Apuser from './Apuser';
import Login from './Login';
import Navbar from './Navbar';
import Tableuser from "./Tableuser";





class App extends Component {
  render() {
    return (
      <Router>
      <div>
        
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
     
        </nav>
        <hr />
        <Switch>
            {/* <Route  path='/' component={Login} /> */}
            <Route path='/Apuser' component={Apuser} />
            <Route path='/' component={Home} />
            <Route path='/Navbar' component={Navbar} />
            <Route path='/Tableuser' component={Tableuser}/>
            
          
        </Switch>
      </div>
    </Router>
    );
  }
}

export default App;