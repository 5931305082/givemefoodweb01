import React from 'react';

export default class Hideable extends React.Component{
    constructor (props) {
        super(props);
            this.state ={
                isHidden: false,
            }
        }
         toggleIsHidden(){
             this.setSate((currenState)=>({
                 isHidden: !currenState.isHidden,
             }));
         }


         render () {
             return(
                 <div>
                     <button onClick={() => this.toggleIsHidden()}></button>>
                     {!this.state.isHidden && this.props.text}
                 </div>
             );
         }
    }    
    
