import React, { Component } from 'react';
import logo1 from './logo1.png';
import firebase, { auth } from './firebase';
import Navbar from './Navbar';

import Apuser from './Apuser';
import Home from './Home';

import { Modal, ModalDialog } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


import './App.css';

class Tableuser extends Component {

  constructor() {
    super();
    this.state = {
      resname: [],
      filter: [],
      items: [],
      User: [],
      ResOwner: [],
      username: [],
      status: [],
      item_id: '',
      title: '',
      name: [],
      description: '',
      address: '',
      email: [],//add email state
      password: [],//add password state
      isLogin: true, //add isLogin state
      showView: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)

  }


  componentDidMount() {

    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user })
      }
    })

    const itemsRef = firebase.database().ref('items');
    itemsRef.on('value', (snapshot) => {
      let items = snapshot.val();
      let newState = [];
      for (let item in items) {
        newState.push({
          item_id: item,
          title: items[item].title,
          description: items[item].description
        })
      }
      this.setState({
        items: newState
      })
    })
      

    const UserRef = firebase.database().ref('User');
    UserRef.on('value', (snapshot) => {
      let User = snapshot.val();
      let newState = [];
      for (let item in User) {
        newState.push({
          item_id: item,
          name: User[item].name,
          email: User[item].email,
          username: User[item].username,
          address: User[item].address,
          status: User[item].status

        })
      }
      this.setState({
        User: newState
      })
    })
    const filterRef = firebase.database().ref('Filter');
    filterRef.on('value', (snapshot) => {
      let filter = snapshot.val();
      let newState = [];
      for (let item in filter) {
        newState.push({
          item_id: item,
          title: filter[item].title,
          description: filter[item].description
        })
      }
      this.setState({
        filter: newState
      })
    })

  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }


  handleSubmit(e) {
    e.preventDefault();

    if (this.state.item_id !== '') {
      return this.updateItem();
    }

    const itemsRef = firebase.database().ref('User')
    const item = {
      name: this.state.name,
      username: this.state.username,
      email: this.state.email,
      address: this.state.address,
      status: this.state.status
    }
    itemsRef.push(item)
    this.setState({
      item_id: '',
      name: '',
      username: '',
      email: '',
      address:'',
      status:''
    })
  }

  handleUpdate = (item_id = null, name = null, username = null, email = null,
    address = null, status = null) => {
    this.setState({ item_id, name, username, email, address, status })
  }

  updateItem() {

    var obj = { name: this.state.name, username: this.state.username, email: this.state.email, address: this.state.address, status: this.state.status }

    const itemsRef = firebase.database().ref('/User')

    itemsRef.child(this.state.item_id).update(obj);

    this.setState({
      item_id: '',
      name: '',
      username: '',
      email: '',
      address: '',
      status: ''
    })

  }

  removeItem(itemId) {
    const itemsRef = firebase.database().ref('/items');
    itemsRef.child(itemId).remove();
  }

  updateItem() {

    var obj = { title: this.state.title, description: this.state.description }

    const itemsRef = firebase.database().ref('/resname')

    itemsRef.child(this.state.item_id).update(obj);

    this.setState({
      item_id: '',
      title: '',
      description: ''
    })

  }

  removeItem(itemId) {
    alert("Remove " + itemId)
    const itemsRef = firebase.database().ref('User');
    itemsRef.child(itemId).remove();
  }

  viewItem() {
    alert("viewItem ")
  }
  // removeItem1(itemId) {
  //   const itemsRef = firebase.database().ref('/resname');
  //   itemsRef.child(itemId).remove();
  // }

  login = () => {

    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then(() => {

      this.setState({ isLogin: true });

    }).catch(function (error) {
      var errorCode = error.code;
      var errorMessage = error.message;
      if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
    });
  }

  logout() {
    auth.signOut().then(() => {
      this.setState({ isLogin: false })
    })
  }


  loginForm() {
    return (
      <div>
        <main role="main" className="container " style={{ marginTop: 80 }}>
          <div className="container img">
            <img src={logo1} width="500px" height="325px" alt="A logo1" alignitem />
          </div>
          <div className="row">
            <div className="col-4"></div>
            <div className="col-4">
              <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" name="email" onChange={this.handleChange} value={this.state.email} placeholder="Enter email" />
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="password" onChange={this.handleChange} value={this.state.password} placeholder="Password" />
                </div>
                <button class="btn btn-primary" onClick={() => this.login()}>Log In</button>
              </form>
            </div>
          </div>
        </main>
      </div>
    )
  }


  toggleIsHidden() {
    this.setSate((currenState) => ({
      isHidden: !currenState.isHidden,
    }));
  }

  showViewModal(name) {
    this.setState({
      showView: true,
      name: name
    })
  }

  closeViewModal() {
    this.setState({
      showView: false
    })
  }


  render() {

    if (!this.state.isLogin) {
      return this.loginForm()
    }
    return (

      <div>

        <nav class="navbar naavbar-light bg-warning fixed-top second-navbar">
          <ul className="navbar-nav mr-auto add-rmargin"></ul>

          {/* <img src={logo1} width="250px" height="150px" alt="A logo1" /> */}
          {/* <div className="add-rmargin"> */}
          <Link to={'./Home'} className="nav-link">User Manage</Link>
          <Link to={'./Apuser'} className="nav-link">Restaurant Manage</Link>
         <Link to={'./Tableuser'} className="nav-link">Add Admin</Link>

       


        </nav>

        <nav class="navbar  fixed-top nav-bg ">
          <img src={logo1} width="150px" height="70px" alt="A logo1" />

          <span class="navbar-brand mb-0 h1" className="App-title" style={{ fontFamily: 'Lobster' }}>Givemefood</span>
          {
            this.state.isLogin ? <button className="btn btn-danger" onClick={() => this.logout()} >Logout</button> : null
          }

        </nav>
        <div className="container" style={{ marginTop: 150 }}>

          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-8">
                <div className="form-row">

                </div>
              </div>
            </div>
          </form>
          <hr />


          <table className="table table-sm table-bordered">


            {
              this.state.filter.map((item) => {
                return (
                  <tr>
                    <td>{item.title}</td>
                    <td>{item.description}</td>

                    <td><button className="btn btn-warning btn-sm" onClick={() => this.handleUpdate(item.item_id, item.title, item.description)}>ADD</button></td><td><button className="btn btn-danger btn-sm" onClick={() => this.removeItem(item.item_id)}>ADD</button></td>
                    <td><button className="btn btn-danger btn-sm" onClick={() => this.removeItem(item.item_id)}>Delete</button></td>
                  </tr>
                )
              })
            }
          </table>

         

          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-8">
                <div className="form-row">
                  <div className="col-4">
                    <input type="text" name="name" className="form-control" placeholder="Name" onChange={this.handleChange} value={this.state.name} />
                  </div>
                  <div className="col-6">
                    <input type="text" name="username" className="form-control" placeholder="User Name" onChange={this.handleChange} value={this.state.username} />
                  </div>
                  <div className="col-6">
                    
                    <input type="text" name="email" className="form-control" placeholder="Email" onChange={this.handleChange} value={this.state.email} />
                  </div>
                  <div className="col-6">
                  
                    <p><input type="text" name="address" className="form-control" placeholder="Address" onChange={this.handleChange} value={this.state.address} />
                    </p>
                  </div>
                  <div className="col-6">
                   
                    <p><input type="text" name="status" className="form-control" placeholder="Status" onChange={this.handleChange} value={this.state.status} />
                    </p>
                  </div>
                  <div className="col">
                    <p></p>
                    <p></p>
                    <button class="btn btn-primary" > Confirm</button>
                  </div>
                </div>
              </div>
            </div>
          </form>






          {/* <form  onSubmit={this.handleSubmit}>
            <div className="row">
                <div className="col-8">
                  <div className="form-row">
                    <div className="col-4">
                    <p>Name</p>
                      <input type="text" name="title" className="form-control" placeholder="Name" onChange={this.handleChange} value={this.state.title}/>
                    </div>
                      <div className="col-6">
                      <input type="text" name="description" className="form-control" placeholder="Phone Number" onChange={this.handleChange} value={this.state.description}/>
                    </div>
                    <div className="col-6">
                    <p>Phone Number</p>
                      <input type="text" name="description" className="form-control" placeholder="Phone Number" onChange={this.handleChange} value={this.state.description}/>
                    </div>
                    <div className="col">
                          <button class="btn btn-primary" > Confirm</button>  
                          <button class="btn btn-danger sm" > Cancel</button>    
                    </div>
                  </div>
                </div>
            </div>
          </form> */}



        </div>



      </div>


    );
  }
}

export default Tableuser;